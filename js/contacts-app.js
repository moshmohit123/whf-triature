  var app = angular.module("contact-app", ['ui.router', 'ngSanitize', 'whfcommon']);

  app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

            }]);


  app.controller("ContactCtrl", ["$scope", "$http", "$window", "$location", "$anchorScroll", function ($scope, $http, $window, $location, $anchorScroll) {
      $scope.activeMenu = 'Contact';
       $('.lazy').lazyload({
          threshold: 150
      });
      $('#phone').keyup(function () {
          this.value = this.value.replace(/[^0-9\.]/g, '');
      });
      $("#phone").on('keydown', function (e) {
          var deleteKeyCode = 8;
          var backspaceKeyCode = 46;
          var tabKeyCode = 9;
          if ((e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105) || e.which === deleteKeyCode || e.which === backspaceKeyCode || e.which === tabKeyCode) {

              return true;
          } else {

              return false;
          }
      });
      }]);