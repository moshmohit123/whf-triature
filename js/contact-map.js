 var markers = [
     {
         "title": 'Jaipur',
         "lat": '26.8538',
         "lng": '75.8052',
         "description": 'Jaipur (VKI, Vaishali Nagar,<br/> C-Scheme, WTP)',"isSubLocationAvailable":true,
              "subCities":[{
         "title": 'Jaipur - WTP',
         "lat": '26.853584',
         "lng": '75.804972',
         "description": '620, 6th Floor,<br/> North Block, World Trade Park,<br/> Malviya Nagar, JLN Road,<br/> Jaipur, Rajasthan - 302017'
    },{
         "title": 'Jaipur - C Scheme',
         "lat": '26.910432',
         "lng": '75.807551',
         "description": '209, Axis Mall,<br/> Bhagwan Das Road, Panch Batti,<br/> C Scheme, Ashok Nagar,<br/> Jaipur, Rajasthan - 302001'
    },{
         "title": 'Jaipur - DCM',
         "lat": '26.894126',
         "lng": '75.747118',
         "description": '1st Floor, Plot no. 18,<br/> Bidla Sons Tower, Teacher Colony, Ajmer Road, Near Elements Mall, Vaishali Nagar,<br/> Jaipur, Rajasthan - 302021'
    },{
         "title": 'Jaipur - VKI',
         "lat": '26.977151',
         "lng": '75.773371',
         "description": '2nd Floor, Plot no D 27 (H),<br/> VKI area, Road no 3,4, Sikar Road,<br/> Jaipur, Rajasthan - 302013'
    }]
    },
     {
         "title": 'Ajmer',
         "lat": '26.489916',
         "lng": '74.629845',
         "description": '1st Floor, Plot No 378, Makarwali Road, Opp T Mos Restaurant, Vaishali Nagar, Ajmer, Rajasthan - 305001',
"isSubLocationAvailable":false
    
    },
     {
         "title": 'Alwar',
         "lat": '27.571134',
         "lng": '76.614138',
         "description": '2nd Floor, Krishna Tower, 18, Lajpat Nagar, Scheme No. 02, Near Sethi Children Hospital, Vijay Mandir Road, Alwar, Rajasthan - 301001',
"isSubLocationAvailable":false
    },
     {
         "title": 'Jhunjhunu',
         "lat": '28.1289',
         "lng": '75.3995',
         "description": 'Coming Soon',
"isSubLocationAvailable":false
    },
     {
         "title": 'Kishangarh',
         "lat": '26.586856',
         "lng": '74.860507',
         "description": '2nd Floor, Shop No 6, Sumer City Centre, City Road, Near Lions Circle, Teli Mohalla, Madanganj, Kishangarh, Rajasthan - 305801',
"isSubLocationAvailable":false
    },
     {
         "title": 'Kotputli',
         "lat": '27.705522',
         "lng": '76.197140',
         "description": '1st Floor, HK Tower, Above IDBI Bank, Krishna Talkies Road, Opposite Nagar Palika Park, Village Bachdi, Kotputli, Rajasthan - 303108',
"isSubLocationAvailable":false
    },
     {
         "title": 'Sikar',
         "lat": '27.590748',
         "lng": '75.173026',
         "description": '1st Floor, Ward No. 18 (Old), Above Laxmi Motors, Opposite Circuit House, Khicharo Ka Bas, Jaipur Road, Sikar, Rajasthan - 332001',
"isSubLocationAvailable":false
    },
     {
         "title": 'Tonk',
         "lat": '26.175878',
         "lng": '75.808336',
         "description": 'Ground floor, Arihanta Tower, Chawani Rd, Khandelwal, Tonk, Rajasthan - 304001',
"isSubLocationAvailable":false
    },
     {
         "title": 'Beawar',
         "lat": '26.104882',
         "lng": '74.315052',
         "description": 'Coming Soon',
"isSubLocationAvailable":false
    },
     {
         "title": 'Bikaner',
         "lat": '28.010325',
         "lng": '73.333353',
         "description": '1st Floor, Shop No 19, Panchsati Circle, Sadul Ganj, Bikaner, Rajasthan - 334001',
"isSubLocationAvailable":false
    },
     {
         "title": 'Jodhpur',
         "lat": '26.282308',
         "lng": '73.009615',
         "description": 'Room 1, 2 & 3, 1st Floor, Dhanlaxmi Tower, Shop number 31 to 34, Chopasani Road, Jodhpur, Rajasthan - 342003',
"isSubLocationAvailable":false
    },
     {
         "title": 'Nagaur',
         "lat": '27.197796',
         "lng": '73.755585',
         "description": '1st Floor, Vinayak Tower, Opp. Ganesh Ji Temple, Didwana Road, Nagaur - 341001'
    },
     {
         "title": 'Pali',
         "lat": '25.771194',
         "lng": '73.332276',
         "description": '1st Floor, Nirmal, 51 H, Veer Durga Das Nagar, College Road, Opposite Rotary Club, Pali, Rajasthan - 306401'
    },
     {
         "title": 'Jalore',
         "lat": '25.1257',
         "lng": '72.1416',
         "description": 'Coming Soon',
"isSubLocationAvailable":false
    },
     {
         "title": 'Kota',
         "lat": '25.168238',
         "lng": '75.848774',
         "description": '2nd Floor, Plot No 390, Shopping Center, Rawatbhata Road, Near Ghode Baba Circle, Gumanpura, Kota, Rajasthan - 324007',
"isSubLocationAvailable":false
    },
     {
         "title": 'Baran',
         "lat": '25.103500',
         "lng": '76.502151',
         "description": '1st Floor, Khasra No 453, Panchavati Colony, Opposite Government Hospital, Hospital Road, Baran, Rajasthan - 325205',
"isSubLocationAvailable":false
    },
     {
         "title": 'Bhilwara',
         "lat": '25.362915',
         "lng": '74.628675',
         "description": '2nd floor, Aone Plaza, Above Aone Motors, Near UIT, Ajmer Road, Subhash Nagar, Bhilwara, Rajasthan - 311001',
"isSubLocationAvailable":false
    },
     {
         "title": 'Jhalawar',
         "lat": '24.589371',
         "lng": '76.160517',
         "description": '1st Floor, Plot no 1, Durga Vihar Colony, Opp. SRG Hospital, NH 12, Jhalawar, Rajasthan - 326001',
"isSubLocationAvailable":false
    },
     {
         "title": 'Udaipur',
         "lat": '24.607566',
         "lng": '73.706612',
         "description": '1st Floor, 100 Feet Road, MP enclave, Near Shobhapura Circle, Ratneshwar Colony, Pulla Bhuwana, Shobhagpura, Udaipur, Rajasthan - 313001',
"isSubLocationAvailable":false
    },
     {
         "title": 'Banswara',
         "lat": '23.5461',
         "lng": '74.4350',
         "description": 'Coming Soon',
"isSubLocationAvailable":false
    },
     {
         "title": 'Chittorgarh',
         "lat": '24.881191',
         "lng": '74.634218',
         "description": 'Shop No. 3, Gandhi Nagar, Sec. 4, Ghatyavali Rd, Opposite Pannadhaya Bus Stand, Chittorgarh, Rajasthan - 312001',
"isSubLocationAvailable":false
    },
     {
         "title": 'Rajsamand',
         "lat": '25.059838',
         "lng": '73.882088',
         "description": '1st Floor, Soni Arcade Building, Collectorate Road, Near Rajmahal Hotel, Subhash Nagar Colony, Kankroli Road, Rajsamand, Rajasthan - 313333',
"isSubLocationAvailable":false
    },
     {
         "title": 'Dungarpur',
         "lat": '23.846825',
         "lng": '73.721047',
         "description": '1st Floor, Plot No 16, Above Andhra Bank, Shahstri Colony, College Road, Dungarpur, Rajasthan - 314001',
"isSubLocationAvailable":false
    },
     {
         "title": 'Nimbahera',
         "lat": '24.6257',
         "lng": '74.6810',
         "description": 'Coming Soon',
"isSubLocationAvailable":false
    },
     {
         "title": 'Pratapgarh',
         "lat": '25.9026',
         "lng": '81.7787',
         "description": 'Coming Soon',
"isSubLocationAvailable":false
    },
     {
         "title": 'Sirohi',
         "lat": '24.877031',
         "lng": '72.858202',
         "description": '2nd Floor, Krishna Complex, Near Sanjeevani Hospital, National Highway 168, Mahakal Nagar, Sirohi, Rajasthan - 307001',
"isSubLocationAvailable":false
    }
    ];
 window.onload = function () {
     LoadMap();
 };

var checkTime = function () {
    var now = new Date();
    var messageDiv = document.getElementById('messageDiv');

    var dayOfWeek = now.getDay(); // 0 = Sunday, 1 = Monday, ... 6 = Saturday
    var hour = now.getHours(); // 0 = 12am, 1 = 1am, ... 18 = 6pm
    
    // check if it's a weekday between 9am and 6pm
    if (dayOfWeek > 0 && dayOfWeek < 6 && hour > 8 && hour < 18) {
        messageDiv.innerHTML = 'Open Now';
        messageDiv.className='open-now'; 			
    }
    else {
        messageDiv.innerHTML = 'Closed Now';
        messageDiv.className='closed-now';
    }
};

setInterval(checkTime, 1000);
checkTime();

 function setTextField(ddl) {
     document.getElementById('make_text').value = ddl.options[ddl.selectedIndex].text;     
 }
 var map;
 var marker;
function offsetCenter(latlng, offsetx, offsety) {

    // latlng is the apparent centre-point
    // offsetx is the distance you want that point to move to the right, in pixels
    // offsety is the distance you want that point to move upwards, in pixels
    // offset can be negative
    // offsetx and offsety are both optional

    var scale = Math.pow(2, map.getZoom());

    var worldCoordinateCenter = map.getProjection().fromLatLngToPoint(latlng);
    var pixelOffset = new google.maps.Point((offsetx/scale) || 0,(offsety/scale) ||0);

    var worldCoordinateNewCenter = new google.maps.Point(
        worldCoordinateCenter.x - pixelOffset.x,
        worldCoordinateCenter.y + pixelOffset.y
    );

    var newCenter = map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);

    map.setCenter(newCenter);

}
 
 function LoadMap() {
     var mapOptions = {
         center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
         zoom: 12,
         mapTypeId: google.maps.MapTypeId.ROADMAP
     };
     map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
     
     setTimeout(function(){
         SetMarker(0);
         
     },500);
      
    
 };

var isSubLocationAvailable;
//  for(var i=0;i<20;i++){
//             $('.city'+i).css('display','none');
//       }
 
function SetMarker(position) {
      for(var i=0;i<20;i++){
             $('.city'+i).css('display','none');
         
       }
   isSubLocationAvailable=markers[position].isSubLocationAvailable;
pos=position;
   if(isSubLocationAvailable){
SetSubLocationMarker(0,0);
       for(var i=0;i<20;i++){
             $('.city'+i).css('display','none');
       }
       $('.city'+position).css('display','block');
   }else{
           if (marker != null) {
         marker.setMap(null);
     }

     //Set Marker on Map.
     var data = markers[position];
     var myLatLng = new google.maps.LatLng(data.lat, data.lng);
     
     offsetCenter(myLatLng,200,200);
     
     marker = new google.maps.Marker({
         position: myLatLng,
         map: map,
         title: data.title
     });
             
     
document.getElementById('map_add').innerHTML=data.description;
     //Create and open InfoWindow.
     var infoWindow = new google.maps.InfoWindow();
     infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
     infoWindow.open(map, marker);
   }

 };

function SetSubLocationMarker(parentPosition,position) {
  

           if (marker != null) {
         marker.setMap(null);
     }

     //Set Marker on Map.
     var data = markers[parentPosition].subCities[position];
     var myLatLng = new google.maps.LatLng(data.lat, data.lng);
     
     offsetCenter(myLatLng,200,200);
     
     marker = new google.maps.Marker({
         position: myLatLng,
         map: map,
         title: data.title
     });
             
     
document.getElementById('map_add').innerHTML=data.description;
     //Create and open InfoWindow.
     var infoWindow = new google.maps.InfoWindow();
     infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
     infoWindow.open(map, marker);
   

 };