  var app = angular.module("about-app", ['ui.router', 'ngSanitize', 'whfcommon']);

  app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

            }]);


  app.controller("AboutCtrl", ["$scope", "$http", "$window", "$location", "$anchorScroll", function ($scope, $http, $window, $location, $anchorScroll) {
      
       $('.lazy').lazyload({
          threshold: 150
      });
      
      $scope.activeMenu = 'About';
      $scope.selected1=null;
      $scope.selected2=null;
      $scope.selected3=null;
      
      $scope.tabMgmt = function(m){
          $scope.selected1=m;
        $("#mgmt-dropdown").modal(500);
      }
      
         $scope.indiMgmt = function(i){
          $scope.selected2=i;
        $("#indi-dropdown").modal(500);
      }
         $scope.addMgmt = function(a){
          $scope.selected3=a;
        $("#add-dropdown").modal(500);
      }
        $scope.closeMgmt1 = function(){
         $(".mgmt-dropdown").slideToggle(500);
                   
      }
           $scope.closeMgmt2 = function(){
       $(".indi-dropdown").slideToggle(500);
                   
      }
           $scope.closeMgmt3 = function(){
          $(".add-dropdown").slideToggle(500);   
      }
        
        
        $scope.mgmtData = [{
                            mgmt_id:1,
                            mgmt_name:'Sanjay Singh Rajawat',
                            mgmt_profile:'Director & CEO',
                            mgmt_image:'sanjay',
                            mgmt_desp:' In his short tenure as the whole time Director & CEO, Mr. Sanjay Singh Rajawat has already heralded several innovations at Wonder Home Finance. Marshalling his 25 years of experience as a business leader, Mr. Rajawat aspires to position Wonder Home Finance as a truly committed & differentiated service provider in a highly competitive market. A true believer in the dreams of the common man, Mr. Rajawat’s ambition is to deliver as many loans to those in need in the shortest time possible.With 21 years in the banking sector behind him, Mr. Rajawat expertise extends to many areas such as business strategy & development, operations, sales & marketing, legal & compliance, audits & governance and P & L management. A chartered accountant by education, Mr. Rajawat also holds graduate, postgraduate & doctorate degrees in commerce. Prior to this, Mr. Rajawat held key positions in the banking industry such IDBI Bank, ICICI Bank, Deutsche Bank and Au Small Finance Bank. In his long illustrious career, his major accomplishments are setting up Retail Banking business in Rajasthan for IDBI Bank, Setting up SME – Trade vertical for ICICI Bank, making key contributions in crafting the successful turnaround story of Deutsche bank India retail business and being a core team member in establishing & successfully launching of AU Small Finance Bank.'
                            },
                           {
                            mgmt_id:2,
                            mgmt_name:'Raunak Singh Mohnot',
                            mgmt_profile:'CFO, Head - Strategy & Marketing',
                            mgmt_image:'raunak',
                            mgmt_desp:'Mr. Raunak Singh Mohnot, who leads the mandate for change adaption, ethics and building business relationships at Wonder Home Finance is single-minded in his ambition - to dominate the housing finance business by building the strongest reputation of trust by providing the best customer experience for obtaining loans through use of cutting edge technology, effective policies & processes, and the highest level of customer support, and to build and maintain the strongest brand in the home finance industry in India.A BCom., MCom.(Pursuing), certified Chartered Accountant & Company Secretary, Mr. Mohnot’s experience in business, accountancy & economics, the last a result of management courses from the famed London School of Economics is one of the most important facets of Wonder Home Finance. In his pivotal role, Mr. Mohnot will lead finance, strategy & marketing and chart the growth path of the company, implementing digitization and handling a rewarding customer experience journey, as well as contributing to major policy making decisions.  '},
             {
                            mgmt_id:3,
                            mgmt_name:'Amit Sarin',
                            mgmt_profile:'Head - Credit & Risk',
                            mgmt_image:'amit',
                            mgmt_desp:'Mr. Sarin, an experienced professional with over 18 years behind him, aims to bring a seamless, simple and rewarding process to customers at Wonder Home Finance, and build a reputation of trust and ease. Mr. Sarin’s abilities in strategic planning, sales & marketing, credit underwriting, risk management, business development, channel management, CRM and team management has seen him associated with some of the best financial institutions in the country. In his long line of successes, he has launched locations and attracted business at ING Vysya Bank, Magma Housing Finance and Aspire Home Finance for Rajasthan region & other northern States in last 8 years.' },
                           
               {
                            mgmt_id: 4,
                            mgmt_name:'Sangram Singh',
                            mgmt_profile:'Head - Sales',
                            mgmt_image:'sangram',
                            mgmt_desp:'With over 20 years experience in banking sector spanning the entire range of retail banking with international & national banks, Mr. Sangram Singh aspires to craft the best products in the housing finance industry and transform the housing loan category in India at Wonder Home Finance.With his unparalleled first-hand exposure to a spectrum of banking activities such as retail banking & sales, credit & collection, branch operations, leading CASA & sales teams, RBI compliances and many more, Mr. Singh is one of the key members of the Wonder Home Finance team. '  },
                 {
                            mgmt_id: 5,
                            mgmt_name:'Harish Chandra Sharma',
                            mgmt_profile:'Head - Operations',
                            mgmt_image:'harish',
                            mgmt_desp:'As the head of operations at Wonder Home Finance, Mr. Harish Chandra Sharma is committed to create the most fulfilling and helpful consumer journey for millions of Indians who need home finance loans.Mr. Sharma brings his in-depth expertise & knowledge, spanning over 23 years, to retail asset operations, operating systems & processes, disbursement handling and many more topics. Prior to joining WHFL, Mr. Sharma has been associated with AVIOM, AAVAS Financiers Ltd., Jayalakshmi Financial Services Ltd, HDFC Bank Ltd & ABN Amro Bank.' },
                 {
                            mgmt_id: 6,
                            mgmt_name:'Piyush Jain',
                            mgmt_profile:'Head - Technology & Innovation',
                            mgmt_image:'piyush',
                            mgmt_desp:'As the head of technology at Wonder Home Finance, Mr. Piyush Jain will create, lead and deliver the technology roadmap with business stakeholders and bring the most effective technology to the brand, and also ensure that all process are at par with best practices from around the world. A senior technology leader, his vast experience spans all stages of a company from a startup culture (CarDekho.com) to multinationals (CitiCorp, JP Morgan). Among his many strengths are providing excellent customer experience and optimizing internal efficiencies through technology.'
             },
                 {
                            mgmt_id: 7,
                            mgmt_name:'Shelendra Singh Shekhawat',
                            mgmt_profile:'Head - Human Capital & Infrastructure',
                            mgmt_image:'shelendra',
                            mgmt_desp:'Not only should Wonder Home Finance be the best company in its category, it also should be one of the best places to work at, attracting the best talent in the industry. This is the vision pursued by Mr. Shelendra Singh Shekhawat, who leads HR at Wonder Home Finance.A highly skilled HR professional with almost a decade’s experience of demonstrated expertise, Mr. Shekhawat’s strengths lie in strategic planning, policy & procedure administration, recruiting & hiring practices, development & retention approaches etc.'
             },
                 {
                            mgmt_id: 8,
                            mgmt_name:'Manas Srivastava',
                            mgmt_profile:'Company Secretary',
                            mgmt_image:'manas',
                            mgmt_desp:'As Company Secretary, Mr. Manas Srivastava leads all the important mandate of compliances, and he is committed to create and maintain a zero-error environment.Mr. Srivastava, a Company Secretary since 2013 and previously with Mentor Home Loans India, heads the critical function of ensuring external rules imposed upon an organization as also compliance with internal systems are adhered to.'
             }     
                           
                           ];
      
       $scope.indData = [{
                            ind_id: 1,
                            ind_name:'Ramesh N.G.S',
                            ind_profile:'MD & CEO - StockHolding',
                            ind_image:'Ramesh',
                            ind_desp:'With a mission of being a ‘world-class’, ‘technology driven’ and ‘client focussed’ market leader in financial and technical services, Mr. Ramesh is a Managing Director & CEO of Stock Holding Corporation of India Limited (StockHolding). Also, Mr. Ramesh is a Chairman on the Boards of StockHolding’s subsidiary companies (100% holding) viz. SHCIL Services Limited and StockHolding DMS Ltd. and Trustee on the Boards of SHCIL Foundation, SHCIL Gratuity & Super Annuation Trust. Prior to joining StockHolding, Mr. Ramesh worked with IDBI Bank, HDFC Bank, Times Bank, IndusInd Bank & Syndicate Bank. He holds a Bachelor’s Degree in Science from University of Pune and also completed a Post-Graduate Diploma in Investment & Financial Management from the University of Pune. He Has an overall experience of 30 + years in Retail Business, Resource PMS & Training, HR, Operations and Vigilance.'
       },
                          {
                            ind_id: 2,
                            ind_name:'D C Jain',
                            ind_profile:'CEO - IIFL Asset Reconstruction Ltd',
                            ind_image:'DCJain',
			    ind_desp:'Mr. D C Jain currently serves as the CEO of IIFL Asset Reconstruction Limited. He is a qualified CAIIB and Post Graduate in Commerce (Specialization in Business Administration) and a Participant in the prestigious Advanced Management Program at Stanford University. He has over 37 years of rich and varied experience in the banking industry across all major business segments such as retail, corporate, Government, MSME and audit; capped by a successful stint as Managing Director and CEO of a leading investment bank. Additionally, Mr. Jain is an advisor for IIFL Wealth Management Ltd. since Sept 2015. He Is a Board member at M/s Suguna Fincorp Private Limited and SHCIL services Ltd. He also serves as an independent External expert at Yes Bank’s Revival and Rehabilitation of Micro, small and Medium Enterprises committee. Mr. Jain started his career as a Probationary Officer with Union Bank of India in 1978. He worked at IDBI Bank Ltd. from 1997 to April 2014 where he headed all major business segments such as retail, Mid corporate, Government, MSME and audit. After serving IDBI Bank Ltd. as Chief General Manager, he joined IDBI Capital Market Services Ltd. as MD & CEO in 2014. His other past associations include - Member of NSE SME Exchange Committee, Director with Association of Investment Bankers of India; and Nominee Director of IDBI Agricultural and Rural Development Trust (IARDT), Gujarat Institute of Development Research, Gujarat Heavy Chemical Limited and Gujarat Industrial Power Limited.'
             }  
                         
                         ];
      
       $scope.addData = [{
                            add_id: 1,
                            add_name:'Ravindra Singh Mohnot',
                            add_profile:'',
                            add_image:'ravindra',
                            add_desp:'Mr. Ravindra Singh Mohnot, a leading practicing Chartered Accountant and Financial Advisor with expertise in almost all the sectors of financial and management services has been associated with R.K. Group since its inception in 1989. As an advisor to this young company, Mr. Mohnot will deploy his vast experience in the field of finance to mentor Wonder Home Finance to successfully launch its business and reach new heights in a short period. As a true Rajasthani, he aspires to bring the message and the services of Wonder Home Finance to tens of thousands of citizens in his home state, and then eventually, to the millions spread across every corner of the country. That too, with highest standards of ethics & service, that has been his hallmark over decades.Mr. Ravindra Singh Mohnot, a leading practicing Chartered Accountant and Financial Advisor with expertise in almost all the sectors of financial and management services has been associated with R.K. Group since its inception in 1989. As an advisor to this young company, Mr. Mohnot will deploy his vast experience in the field of finance to mentor Wonder Home Finance to successfully launch its business and reach new heights in a short period. As a true Rajasthani, he aspires to bring the message and the services of Wonder Home Finance to tens of thousands of citizens in his home state, and then eventually, to the millions spread across every corner of the country. That too, with highest standards of ethics & service, that has been his hallmark over decades.'
       },
                          {
                            add_id: 2,
                            add_name:'Tarun Singh Chauhan',
                            add_profile:'',
                            add_image:'tarun',
                            add_desp:'Mr. Tarun Singh Chauhan has been associated with the RK Group for years, and advised the growth trajectory of both RK Marbles as well as Wonder Cement through marketing breakthroughs. Now, Mr. Chauhan aims to bring his years of expertise and abilities to Wonder Home Finance and wants to build up the brand to become a byword for trust, expertise & service and achieve his vision of making Wonder Home Finance one of the most respected brands in the country.A man of many talents, with an illustrious career that spanned cricket to advertising, Mr. Chauhan was associated with some of the world’s best agencies. Nominated as President of Lowe after being with the company for nearly 10 years, Mr. Chauhan’s desire to take on newer and greater challenges, resulted in his return to JWT as Managing Partner. Off late, he launched TSC Consulting, where he has built a reservoir of the best management talent that helps companies, brands and governments optimize, prosper & grow. '
             }  
                         
                         ];
      
      
      
  }]);
