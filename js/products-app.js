  var app = angular.module("products-app", ['ui.router', 'ngSanitize', 'whfcommon']);

  app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

            }]);


  app.controller("ProductsCtrl", ["$scope", "$http", "$window", "$location", "$anchorScroll", function ($scope, $http, $window, $location, $anchorScroll) {
       $scope.activeMenu = 'Products';
        $('.lazy').lazyload({
          threshold: 150
      });
      
      $('.owl-carousel').owlCarousel({
    loop:true,
    autoplay: true,
    dots: true,
    singleItem:true,
    margin:0,
    items:1,
    lazyLoad:true,
});
        }]);