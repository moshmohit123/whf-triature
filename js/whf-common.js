var commom = angular.module('whfcommon', []);
commom.directive('whfFooter', function () {
    return {

        restrict: 'E',
        controller: function ($scope, $rootScope, $http) {
            new WOW().init();

        },
        templateUrl: 'whf-footer'
    };
});

commom.directive('whfHeader', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, $anchorScroll, $location) {

            $scope.otp = 1;
            $scope.setActiveMenu = function (p) {
                $scope.activeMenu = p;
            }

            $scope.loaded = true;
            $scope.text = 'Nothing loaded yet.';

            $scope.loadSomething = function () {
                $scope.loaded = false;

                $timeout(function () {
                    // Simulates loading
                    $scope.text = 'Hello World';
                    $scope.loaded = true;
                }, 2000);
            };

            $scope.hamb = function () {
                $('body').toggleClass("fixedPosition");
            }

            $(window).scroll(function () {
                var sticky = $('.sticky'),
                    scroll = $(window).scrollTop();

                if (scroll >= 175) {
                    sticky.addClass('header-fixed');
                    
                    $('.whf-header--XS').addClass('header-fixed-xs');
                    $('.whf-xs-header-menu').addClass('whf-xs-header-menu--scroll');
                    $('.whf-header--right__logo').addClass('whf-header--right__logo-sticky');
                    $('.whf-header--li__menu').addClass('whf-header--li__menu--sticky');
                    $('.whf--submenu__ul').addClass('whf--submenu__ul--sticky');
                } else {
                    $('.whf-header--XS').removeClass('header-fixed-xs');
                    $('.whf-xs-header-menu').removeClass('whf-xs-header-menu--scroll');
                    $('.whf-header--right__logo').removeClass('whf-header--right__logo-sticky');
                   
                    sticky.removeClass('header-fixed');
                    $('.whf-header--li__menu').removeClass('whf-header--li__menu--sticky');
                    $('.whf--submenu__ul').removeClass('whf--submenu__ul--sticky');

                }
            });
        },
        templateUrl: 'whf-header'
    };
});