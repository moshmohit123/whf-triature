  var app = angular.module("index-app", ['rzModule', 'ui.router', 'ngSanitize', 'whfcommon']);

  app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {


            }]);

  app.controller("IndexCtrl", ["$scope", "$http", "$window", "$location", "$anchorScroll", "$timeout", function ($scope, $http, $window, $location, $anchorScroll, $timeout) {

      $('.lazy').lazyload({
          threshold: 150
      });
      $scope.small = 'none';

      /*  if ($(window).width() < 480) {
            $scope.small = 'yes';
            if ($scope.small == 'yes') {
                $timeout(function(){ 
                $('.owl-xs').owlCarousel({
                    loop: true,
                    autoplay: true,
                    dots: true,
                    singleItem: true,
                    margin: 0,
                    items: 1,
                    lazyLoad: true,
                });
                    },1000);
            }
        }
       if ($(window).width() > 480) {
            $scope.small = 'no';
            if ($scope.small == 'no') {
                $timeout(function(){ 
                    $('.owl-carousel .owl-lg').owlCarousel({
                    loop: true,
                    autoplay: true,
                    dots: true,
                    singleItem: true,
                    margin: 0,
                    items: 1,
                    lazyLoad: true,
                });
                },1000);
            }
        }*/
      
      var owl1 = $('.owl-lg');

      owl1.owlCarousel({
          loop: true,
          lazyLoad: true,
          autoplay: true,
          items: 1,
          nav: false,
          center: true,
          dots: true,
          margin: 0
      });
      var owl2 = $('.owl-xs');

      owl2.owlCarousel({
          loop: true,
          lazyLoad: true,
          autoplay: true,
          items: 1,
          nav: false,
          center: true,
          dots: true,
          margin: 0
      });

      $('#carouselFade').carousel();
      $scope.myChangeListener = function (sliderId) {


          /*EMI per month*/
          $scope.e = Math.round((0.50 * $scope.slider0.value) - $scope.slider6.value);

          /*I earn per month*/
          $scope.p1 = $scope.slider0.value;

          /*Number of years for EMI*/
          $scope.n = $scope.slider1.value * 12;

          /*Rate of interest for EMI*/
          $scope.r = $scope.slider2.value / 1200;

          /*Loan amount*/
          $scope.p2 = $scope.slider3.value;

          /*Number of years for Loan*/
          $scope.n2 = $scope.slider4.value * 12;

          /*Rate of interest for Loan*/
          $scope.r2 = $scope.slider5.value / 1200;


          $scope.loan = Math.round([$scope.e * (Math.pow((1 + $scope.r), $scope.n) - 1)] / [Math.pow((1 + $scope.r), $scope.n) * $scope.r]);

          $scope.emi = Math.round([$scope.p2 * $scope.r2 * (Math.pow((1 + $scope.r2), $scope.n2))] / [Math.pow((1 + $scope.r2), $scope.n2) - 1]);

          if ($scope.emi > 999 && $scope.emi < 100000) {

              $scope.emi = Math.round($scope.emi / 1000);


              $scope.emi = $scope.emi + " Thousand";
          }

          if ($scope.loan > 99999 && $scope.loan < 10000000) {

              $scope.loan = Math.round($scope.loan / 100000);
              if ($scope.loan == 1) {
                  $scope.loan = $scope.loan + " Lac";
              } else {
                  $scope.loan = $scope.loan + " Lacs";
              }

          }
          if ($scope.loan > 9999999) {
              $scope.loan = Math.round($scope.loan / 10000000);
              if ($scope.loan == 1) {
                  $scope.loan = $scope.loan + " Crore";
              } else {
                  $scope.loan = $scope.loan + " Crores";
              }



          }
      };

      $scope.calc = 1;
      $scope.slider0 = {
          value: 100000,
          options: {
              floor: 10000,
              ceil: 1000000,
              precision: 2,
              onChange: $scope.myChangeListener
          }
      };
      $scope.slider1 = {
          value: 11.5,
          options: {
              floor: 3,
              ceil: 30,
              precision: 2,
              onChange: $scope.myChangeListener
          }
      };
      $scope.slider2 = {
          value: 11,
          options: {
              floor: 4,
              ceil: 18,
              precision: 2,
              onChange: $scope.myChangeListener
          }
      };
      $scope.slider6 = {
          value: 0,
          options: {
              floor: 0,
              ceil: 100000,
              precision: 2,
              onChange: $scope.myChangeListener
          }
      };
      $scope.slider3 = {
          value: 500000,
          options: {
              floor: 25000,
              ceil: 4000000,
              precision: 2,
              onChange: $scope.myChangeListener
          }
      };
      $scope.slider4 = {
          value: 11,
          options: {
              floor: 3,
              ceil: 30,
              precision: 2,
              onChange: $scope.myChangeListener
          }
      };
      $scope.slider5 = {
          value: 11,
          options: {
              floor: 4,
              ceil: 18,
              precision: 2,
              onChange: $scope.myChangeListener
          }
      };



      $scope.e = Math.round((0.50 * $scope.slider0.value) - $scope.slider6.value);

      /*I earn per month*/
      $scope.p1 = $scope.slider0.value;

      /*Number of years for EMI*/
      $scope.n = $scope.slider1.value * 12;

      /*Rate of interest for EMI*/
      $scope.r = $scope.slider2.value / 1200;

      /*Loan amount*/
      $scope.p2 = $scope.slider3.value;

      /*Number of years for Loan*/
      $scope.n2 = $scope.slider4.value * 12;

      /*Rate of interest for Loan*/
      $scope.r2 = $scope.slider5.value / 1200;

      $scope.loan = Math.round([$scope.e * (Math.pow((1 + $scope.r), $scope.n) - 1)] / [Math.pow((1 + $scope.r), $scope.n) * $scope.r]);

      $scope.emi = Math.round([$scope.p2 * $scope.r2 * (Math.pow((1 + $scope.r2), $scope.n2))] / [Math.pow((1 + $scope.r2), $scope.n2) - 1]);
      if ($scope.emi > 999 && $scope.emi < 100000) {

          $scope.emi = Math.round($scope.emi / 1000, 2);
          $scope.emi = $scope.emi + " Thousand";
      }



      if ($scope.loan > 99999 && $scope.loan < 10000000) {

          $scope.loan = Math.round($scope.loan / 100000, 2);
          if ($scope.loan == 1) {
              $scope.loan = $scope.loan + " Lac";
          } else {
              $scope.loan = $scope.loan + " Lacs";
          }
      }
      if ($scope.loan > 9999999) {
          $scope.loan = Math.round($scope.loan / 10000000, 2);
          if ($scope.loan == 1) {
              $scope.loan = $scope.loan + " Crore";
          } else {
              $scope.loan = $scope.loan + " Crores";
          }

      }

      $scope.calcTab = function (tabName) {
          if (tabName == 'tab1') {
              $scope.calc = 1;
              $('.home--loan').addClass('home-loan--tab-active');
              $('.home--emi').removeClass('home-emi--tab-active');
          } else if (tabName == 'tab2') {
              $scope.calc = 2;
              $('.home--emi').addClass('home-emi--tab-active');
              $('.home--loan').removeClass('home-loan--tab-active');
          }
      }

        }]);